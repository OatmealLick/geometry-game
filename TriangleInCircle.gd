extends Node2D

onready var a1: Node2D = $A1
onready var a2: Node2D = $A2
onready var a3: Node2D = $A3

export(float) var delta_distance := 50.0

export(Vector2) var O := Vector2.ZERO
export(float) var R := 0.0

var holded_object: Node2D = null


func _ready():
	var A = a1.global_position
	var B = a2.global_position
	var C = a3.global_position
	
	var AC_normalized = (C - A).normalized() 
	var AB_normalized = (B - A).normalized() 
	var cosAlpha = AC_normalized.dot(AB_normalized)
	var sinAlpha = sqrt(1 - pow(cosAlpha, 2))
	R = (C - B).length() / (2 * sinAlpha)
	
	var D = Vector2((B.x + A.x) / 2, (B.y + A.y) / 2)
	var DO_length = sqrt(pow(R, 2) - (A - D).length_squared())
	var BA_normalized = (A - B).normalized()
	var DO_direction = BA_normalized.rotated(deg2rad(90))
	O = D + (DO_direction * DO_length)
	update()


func _draw():
	var colors = PoolColorArray([Color.aliceblue])
	var points = PoolVector2Array([
		a1.position, a2.position, a3.position
	])
	draw_polygon(points, colors)
	draw_circle(to_local(O), 10.0, Color.red)
	if holded_object:
		draw_line(to_local(O), holded_object.position, Color.rebeccapurple, 5.0)
	

func _process(delta):
	if Input.is_action_pressed("click"):
		var click_position = get_global_mouse_position()
		
		if holded_object:
			var others = _get_others(holded_object)
			holded_object.global_position = _compute_position(click_position)
			update()
		else:
			holded_object = _vertex_clicked_or_null(click_position)
			
	if Input.is_action_just_released("click"):
		holded_object = null
		update()

func _compute_position(click_position: Vector2):

	var direction = (click_position - O).normalized()
	var OP = direction * R
	return O + OP


func _vertex_clicked_or_null(click_position):
	var vertices = [a1, a2, a3]
	var lowest_distance_vertex = null
	var distances = {}
	for vertex in vertices:
		var distance = (vertex as Node2D).global_position.distance_to(click_position)
		if distance <= delta_distance:
			lowest_distance_vertex = vertex
	
	return lowest_distance_vertex


func _line_through_two_points(m: Vector2, n: Vector2):
	var a = (m.y - n.y) / (m.x - n.x)
	var b = m.y - a * m.x
	return Vector2(a, b)
	
func _line_through_one_point(point: Vector2, a: float):
	var b = -a * point.x + point.y
	return Vector2(a, b)
	
func _linear(x: float, factors: Vector2):
	# y = ax + b
	return factors.x * x + factors.y


func _get_others(clicked: Node2D):
	var vertices = [a1, a2, a3]
	vertices.erase(clicked)
	return vertices
