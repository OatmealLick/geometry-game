extends Node2D

onready var a1: Node2D = $A1
onready var a2: Node2D = $A2
onready var a3: Node2D = $A3

export(float) var delta_distance := 50.0

var holded_object: Node2D = null

func _draw():
	var colors = PoolColorArray([Color.aliceblue])
	var points = PoolVector2Array([
		a1.position, a2.position, a3.position
	])
	draw_polygon(points, colors)

func _process(delta):
	if Input.is_action_pressed("click"):
		var click_position = get_global_mouse_position()
		
		if holded_object:
			holded_object.global_position = click_position
			update()
		else:
			holded_object = _vertex_clicked_or_null(click_position)
			
	if Input.is_action_just_released("click"):
		holded_object = null


func _vertex_clicked_or_null(click_position):
	var vertices = [a1, a2, a3]
	var lowest_distance_vertex = null
	var distances = {}
	for vertex in vertices:
		var distance = (vertex as Node2D).global_position.distance_to(click_position)
		if distance <= delta_distance:
			lowest_distance_vertex = vertex
	
	return lowest_distance_vertex
