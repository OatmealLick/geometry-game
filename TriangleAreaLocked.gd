extends Node2D

onready var a1: Node2D = $A1
onready var a2: Node2D = $A2
onready var a3: Node2D = $A3

export(float) var delta_distance := 50.0

var holded_object: Node2D = null

func _draw():
	var colors = PoolColorArray([Color.aliceblue])
	var points = PoolVector2Array([
		a1.position, a2.position, a3.position
	])
	draw_polygon(points, colors)

func _process(delta):
	if Input.is_action_pressed("click"):
		var click_position = get_global_mouse_position()
		
		if holded_object:
			var others = _get_others(holded_object)
			var pos = _compute_position(click_position, holded_object.global_position, 
			others[0].global_position, others[1].global_position)
			print("before: %s", holded_object.global_position)
			print("after: %s", pos)
			holded_object.global_position = pos
			update()
		else:
			holded_object = _vertex_clicked_or_null(click_position)
			
	if Input.is_action_just_released("click"):
		holded_object = null

func _compute_position(click_position: Vector2, current_position: Vector2, 
other_position_1: Vector2, other_position_2: Vector2):
	
	var B = other_position_1
	var A = other_position_2
	var C = current_position
	var M = click_position
	
	# 1 project BC onto BA = BD
	var BA = (A - B).normalized()
	var BC = C - B
	var BD = BC.project(BA)
	
	# 2 B + BD = D
	var D = B + BD
	
	# 3 having DC, N2 = N1 + DC
	var N1 = Vector2(BA.y, -BA.x)
	var DC = C - D
	var N2 = N1 + DC
	
	# 4 rotate N2
	var N2rotated = Vector2(-N2.y, N2.x)
	
	# 5 CP
	var CP = (M - C).project(N2rotated)
	
	var P = C + CP
	return P

func _vertex_clicked_or_null(click_position):
	var vertices = [a1, a2, a3]
	var lowest_distance_vertex = null
	var distances = {}
	for vertex in vertices:
		var distance = (vertex as Node2D).global_position.distance_to(click_position)
		if distance <= delta_distance:
			lowest_distance_vertex = vertex
	
	return lowest_distance_vertex


func _line_through_two_points(m: Vector2, n: Vector2):
	var a = (m.y - n.y) / (m.x - n.x)
	var b = m.y - a * m.x
	return Vector2(a, b)
	
func _line_through_one_point(point: Vector2, a: float):
	var b = -a * point.x + point.y
	return Vector2(a, b)
	
func _linear(x: float, factors: Vector2):
	# y = ax + b
	return factors.x * x + factors.y


func _get_others(clicked: Node2D):
	var vertices = [a1, a2, a3]
	vertices.erase(clicked)
	return vertices
